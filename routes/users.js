var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var scrfProtection = csrf();
var passport = require('passport');


/* GET users listing. */
router.use(scrfProtection);

router.get('/logout', isLoggedIn, function (req, res) {
    req.logout();
    res.redirect('/');
});

router.use('/', notLoggedIn, function (req, res, next) {
    res.locals.showBGImage = true;
    next();
});
router.post('/login', passport.authenticate('local.signin', {
    successRedirect : '/',
    failureRedirect : '/user/login',
    failureFlash : true
}));
router.get('/login', function(req, res, next) {
    var messages = req.flash('error');
    var isError = (messages.length>0);
  res.render('user/login',{title : 'MyBlog | Login', csrfToken : req.csrfToken(), messages : messages, iserror : isError});
});
router.get('/signup', function(req, res, next) {
    var messages = req.flash('error');
    var isError = (messages.length>0);
  res.render('user/signup',{title : 'MyBlog | Signup', csrfToken : req.csrfToken(), messages : messages, iserror : isError});
});
router.post('/signup', passport.authenticate('local.signup',{
    successRedirect : '/',
    failureRedirect : '/user/signup',
    failureFlash : true
}));

module.exports = router;
function isLoggedIn(req, res, next) {
    if(req.isAuthenticated())
        return next();
    else
        res.redirect('/');
}

function notLoggedIn(req, res, next) {
    if(!req.isAuthenticated())
        return next();
    else
        res.redirect('/');
}