var express = require('express');
var router = express.Router();
var path = require('path');
var Post = require('../models/post');
var Like = require('../models/like');
/* GET home page. */
router.get('/',setLandingPage, function(req, res, next) {
    var content_length = 100;
    Post.getAllPosts(function (err, posts) {
        if(!posts || err || posts.length==0)
            res.render('index', {title: 'Welcome to MyBlog!', posts : {}});
        var result = posts.map(function (post) {
            var data = post;
            data.username = post.user.name;
            data.userid = post.user._id;
            var content = post.content.substr(0, content_length);
            if(content.length>=content_length)
                content = content+'...';
            data.content = content;
            delete(data.user);
            data.images = sortImages(post.images);
            return data;
        });
        k = 0;
        result.forEach(function (row) {
            Like.isLiked(row._id, req.user?req.user._id:null, function (err, islike) {
                row.isliked  = !(!islike || islike.length == 0);
                Like.getLikes(row._id,function (err, c) {
                   if(!c)
                       c=0;
                    row.likes = c;
                    if(k == result.length-1){
                        res.render('index', {title: 'Welcome to MyBlog!', posts : result});
                    }
                    k++;
                });
            });
        });
    });
});

module.exports = router;

function setLandingPage(req, res, next) {
    res.locals.landingPage = true;
    next();
}
function between(x, min, max) {
    return x >= min && x <= max;
}

function sortImages(images) {
    var array = [];
    var i =0;
    if(images.length==0)
        return images;
    for(var val in images){
        array[i]=images[val];
        i++;
    }
    var newimages = array.sort(function(a, b){
        if(between(a.width, 250, 400) && !between(b.width, 250, 400))
            return 1;
        else if(!between(a.width, 250, 400) && between(b.width, 250, 400))
            return -1;
        else if(between(a.width, 250, 400) && between(b.width, 250, 400))
            return a.width - b.width;
        else
            return 0;
    });
    i=0;
    var data = {};
    for(var newval in newimages){
        data[i]=newimages[newval];
        i++;
    }
    return data;
}