var path = require('path');
var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var sizeOf = require('image-size');
var csrfProtection = csrf({ cookie: true });
var multer = require('../config/multer');
var Post = require('../models/post');
var Like = require('../models/like');
/* GET add a blog page. */
router.get('/create',  isLoggedIn, csrfProtection,function(req, res, next) {
    var messages = req.flash('error');
    var isError = (messages.length>0);
    res.render('add', {title: 'Publish your own article', csrfToken : req.csrfToken(),iserror : isError, messages : messages });
});
router.post('/create', isLoggedIn, multer.upload.array('images',10), csrfProtection, function (req, res, next) {
    req.checkBody('title', ' Title is required').notEmpty();
    req.checkBody('content', 'Content is required').notEmpty();

    var errors = req.validationErrors();
    if(req.files.length == 0) {
        if(!errors)
            errors = [];
        errors.push({msg : "Atleast one image is required"});
    }
    if(errors){
        var messages = [];
        for(var j in errors)
            messages.push(errors[j].msg);
        res.json({s:false, error : messages.join('<br/>')});
        req.flash('error',messages);
    }
    var photos = req.files || {};
    var body =req.body || {};
    var images = {};
    var k = 0;
    for(var i in photos){
        var dimensions = sizeOf(path.join('public','uploads',photos[i].filename));
        images[k]={url : path.normalize(path.join('/blog', 'image',photos[i].filename)) , height : dimensions.height , width : dimensions.width};
        k++;
    }
    if(!errors){
        var post = new Post();
        post.title = body.title;
        post.content = body.content;
        post.user = req.user._id;
        post.images = images;
        post.slug = body.title.toLowerCase().replace(' ','-').replace(/[^0-9a-z-]/gi, '')+"-" + Date.now();
        post.save(function (err) {
            if(err){
                res.json({'s': false, 'error' : 'Oops! Somethings went wrong. Please try again after sometime.'});
                //throw err;
            }else
            {
                req.flash('success','The post is successfully created!');
                res.json({s: true, msg: 'The post is successfully created!', redirect: '/blog/'+post.slug});
            }
        });
    }
});
router.get('/image/:name', function(req, res, next) {
    var fs = require('fs');
    fs.readFile(path.join('public','uploads',req.params.name), function(err, data) {
        if (err){
            res.writeHead(404);
            res.end();
        }else {
            res.writeHead(200, {'Content-Type': 'image/jpeg'});
            res.end(data); // Send the file data to the browser.
        }
    });
});

router.get('/:slug', function (req, res, next) {
    var slug = req.params.slug;
    var messages = req.flash('success');
    var isSuccess = (messages.length>0);
    Post.findOne({slug:slug}).populate('user').exec(function (err, post) {
        if(err)
            next();
        if(!post){
            next();
            return false;
        }
        post.username = post.user.name;
        delete(post.user);
        var images = sortImages(post.images);
        console.log(images);
        var len = Object.keys(images).length;
        if(images[len-1].width > 1200 && images[len-1].height>300){
            post.cover = images[len-1];console.log(post.cover.url);
            post.cover.url =post.cover.url.replace(/\\/g,'/');
            images = delete(images[len-1]);
            post.images = images;
            len--;
        }
        if(len<4 && len!==0){
            if(len=1)
                post.hasoneimages= true;
            else
                post.customclass = "grid-size-"+len;
        }
        Like.isLiked(post._id, req.user?req.user._id:null, function (err, islike) {
            post.isliked  = !(!islike || islike.length == 0);
            Like.getLikes(post._id,function (err, c) {
                if(!c)
                    c=0;
                post.likes = c;
                res.render('article', {title: 'MyBlog - '+post.title, post : post, messages : messages, issuccess : isSuccess});
            });
        });
    })
});
router.get('/like', isLoggedIn, function (req, res, next) {
    var mongoose = require('mongoose');
    var postid = req.query.postid;
    var userid = req.user._id;
    Like.isLiked(postid, userid, function (err, data) {console.log(data);
       if(!data || data.length==0){
           var like = new Like();
           like.post = mongoose.Types.ObjectId(postid);
           like.user = userid;
           like.save(function (err) {
               if(err)
                   res.json({s : false, error : "Something went wrong!"});
               else
                   res.json({s:true});
           });
       }else{
           Like.remove({post : mongoose.Types.ObjectId(postid)},function (err, result) {
               if(err)
                   res.json({s : false, error : "Something went wrong!"});
               else
                   res.json({s:true});
           });
       }
    });
});
module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated())
        return next();
    else {
        if (req.xhr)
            res.json({s : false, redirect: '/user/login'});
        else
            res.redirect('/user/login');
    }
}

function between(x, min, max) {
    return x >= min && x <= max;
}

function sortImages(images) {
    var array = [];
    var i =0;
    if(images.length==0)
        return images;
    for(var val in images){
        array[i]=images[val];
        i++;
    }
    var newimages = array.sort(function(a, b){
        if(between(a.width, 250, 400) && !between(b.width, 250, 400))
            return 1;
        else if(!between(a.width, 250, 400) && between(b.width, 250, 400))
            return -1;
        else if(between(a.width, 250, 400) && between(b.width, 250, 400))
            return a.width - b.width;
        else
            return 0;
    });
    i=0;
    var data = {};
    for(var newval in newimages){
        data[i]=newimages[newval];
        i++;
    }
    return data;
}