var mongoose = require('mongoose');
var postSchema = new mongoose.Schema({
    title : {type : String, required : true},
    content : {type : String , required : true},
    images : {type : Object , required : true},
    slug : {type : String, required : true},
    user : {type : mongoose.Schema.Types.ObjectId, required : true, ref : 'User'}
});
postSchema.statics.getAllPosts = function(cb){
     return this.find().populate('user').exec(cb);
};
module.exports = mongoose.model('Post', postSchema );