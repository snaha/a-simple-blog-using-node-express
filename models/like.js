var mongoose = require('mongoose');
var postSchema = new mongoose.Schema({
    post : {type : mongoose.Schema.Types.ObjectId, required : true, ref : 'Post'},
    user : {type : mongoose.Schema.Types.ObjectId, required : true, ref : 'User'}
});
postSchema.statics.isLiked = function(postid, userid, cb){
    /*if(!postid || !userid)
        return false;*/
    return this.findOne({post : mongoose.Types.ObjectId(postid), user : mongoose.Types.ObjectId(userid)}).exec(cb);
};
postSchema.statics.getLikes = function(post_id, cb){
    return this.count({post : post_id},cb);
};
module.exports = mongoose.model('Like', postSchema );
