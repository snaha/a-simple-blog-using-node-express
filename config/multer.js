var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now()+".jpg")
    }
});
module.exports.upload = multer({ storage: storage,
    fileFilter : function (req, file, cb) {
        var patt = new RegExp("image/*");
        if(patt.test(file.mimetype))
            cb(null, true);//accept
        else
            cb(null, false); //reject
        //cb(new Error('I don\'t have a clue!'))
    }
});

