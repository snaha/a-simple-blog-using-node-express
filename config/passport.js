var passport = require('passport');
var User = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id,function(err, user){
        done(err, user);
    });
});

passport.use('local.signup', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
},function (req, email, password, done) {
    req.checkBody('name','Name is required').notEmpty();
    req.checkBody('email','Email is required').notEmpty();
    req.checkBody('email','Invalid Email').isEmail();
    req.checkBody('password','Password is required').notEmpty();
    req.checkBody('confirm_password','Password didn\'t match').equals(password);
    req.checkBody('password','Minimum 4 characters for password').isLength({min : 4});
    var errors = req.validationErrors();
    if(errors){
        var messages = [];
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error',messages));
    }

    User.findOne({email : email}, function (err, user) {
        if(err)
            return done(err);
        if(user)
            return done( null,false, { message: "Email is already used!"});
        var newUser = new User();
        newUser.email = email;
        newUser.name = req.body.name;
        newUser.password = newUser.encryptPassword(password);
        newUser.save(function (err) {
            if(err)
                return done(err);
            return done(null, newUser);
        });
    });
}));

passport.use('local.signin', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
}, function (req, email , password, done) {
    req.checkBody('email').isEmail();
    req.checkBody('password').isLength({min : 4});
    var errors = req.validationErrors();
    if(errors){
        var error_msg = [];
        errors.forEach(function (err) {
            error_msg.push(err.msg);
        });
        return done ( null , false , req.flash('error', error_msg));
    }
    User.findOne({email : email}, function (err, user) {
        if(err)
            return done(err);
        if(user){
            if(user.validatePassword(password))
                return done( null , user);
            else
                return done( null ,false, req.flash('error',['Invalid Password!']));
        }else
            return done( null, false , req.flash('error', ['Invalid Email / Password']));
    });
}));