$(function () {
    //set image wrapper height , required for masonry computation
    $('img.img-responsive').each(function () {
        var img = $(this);
        var width = parseInt(img.data('width'),10);
        var height = parseInt(img.data('height'),10);
        var parent_width = $(this).parent().outerWidth(true);
        var new_height;
        if(!parent_width || parent_width==0)
            new_height = 200;//default
        else
            new_height= height/width*parent_width;
        $(this).parent().css('height', new_height+'px');
    });
    //masonry -> checkout :http://masonry.desandro.com/
    $('.grid').masonry({
        itemSelector: '.grid-item',//card selector
        columnWidth:".grid-sizer",//column size set by css
        percentPosition: true
    }).removeClass('hid');//lazy load animation

   $(document).on('click','.more-details .like-btn',function () { // liking a post
      var postid = $(this).data('id');
       var liked = $(this).hasClass('liked');//is already liked?
       var count = parseInt($(this).data('count'),10);// # of like
       var t = $(this);
       $(this).attr('disable','disabled');
       $.get('/blog/like',{ "postid" : postid},function (data) {
           if(data.s){
               if(liked){
                   t.removeClass('liked').text('Like').data('count',count-1);
                   t.closest('.more-details').find('#like-count').text(count-1);
                   t.closest('.thumbnail').find('#like-count').text(count-1);
               }else{
                   t.addClass('liked').text('Liked').data('count',count+1);
                   t.closest('.more-details').find('#like-count').text(count+1);
                   t.closest('.thumbnail').find('#like-count').text(count+1);
               }
           }else if(data.redirect){//login redirect
               window.location = data.redirect;
           }else if(data.error)
               alert(data.error);
       });
   });
});

$(document).ready(function() {

    $('#start_post').submit(function() {//start a post form, post data to /blog/create
        $(this).find('button[type="submit"]').attr('disable','disabled');
        $(this).ajaxSubmit({

            error: function(xhr) {
                $(this).find('button[type="submit"]').attr('disable','');
                console.log( xhr);
                $("#error").empty().text("Server is not responding, please try after sometime!");
            },

            success: function(response) {
                $(this).find('button[type="submit"]').attr('disable','');
                if(response.redirect)
                    window.location = response.redirect;
                if(response.s)
                    $("#success").empty().text(response.msg || "");
                else
                    $("#error").empty().html(response.error || "");
            }
        });
        return false;
    });
});
function handleFileSelect(evt) {// called from add.hbs, for image upload
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
                // Render thumbnail.
                var span = document.createElement('span');
                span.innerHTML = ['<div class ="rel in-block"><img class="thumb" src="', e.target.result,
                    '" title="', escape(theFile.name), '"/><div class="abs img-alt">',escape(theFile.name),'</div></div>'].join('');
                document.getElementById('list').insertBefore(span, null);
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
    }
}