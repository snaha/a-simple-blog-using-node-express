var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expresshbs = require('express-handlebars');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var mongoose = require('mongoose');
var validator = require('express-validator');
mongoose.connect('localhost:27017/myblog');

var index = require('./routes/index');
var users = require('./routes/users');
var post = require('./routes/post');
var notfound = require('./routes/404');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', expresshbs({defaultLayout : 'layout', extname : '.hbs'}));
app.set('view engine', '.hbs');


// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());
app.use(cookieParser());
app.use(session({secret : "sfsdgsdg", resave :false, saveUninitialized : false}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
require('./config/passport');
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
   res.locals.login = req.isAuthenticated();
  next();
});
app.use('/blog', post);
app.use('/user', users);
app.use('/', index);
app.use('/:any', notfound);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
